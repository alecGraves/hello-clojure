(ns hello-clojure.core-test
  (:require [clojure.test :refer :all]
            [hello-clojure.code :refer :all]))

(deftest encode-decode
  (testing "Does (decode(encode input)) = input?"
    (is (= (decode (encode "Hi, AbCdEfG!")) "Hi, AbCdEfG!"))))

(deftest alphabet-correct
  (testing "Is the alphabet correct?"
    (is (= (nth alphabet 0) \a))))
