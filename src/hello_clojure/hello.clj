(ns hello-clojure.hello)

(defn foo
  "I don't do a whole lot."
  [x]
  (clojure.string/join " " [x "Hello, World!"]))

(def say-hi (fn [] (println (foo "Yo!"))))

