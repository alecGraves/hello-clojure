(ns hello-clojure.core
  (:require hello-clojure.code)
  (:require hello-clojure.hello))

(defn -main [& args]
  (hello-clojure.hello/say-hi)
  (println "I will encode/decode for you. What is your input?")
  (let [input (read-line)]
    (println "converted:" (hello-clojure.code/encode input))))
