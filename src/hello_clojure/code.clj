(ns hello-clojure.code
  (:require clojure.set))

(def alphabet (vec (map #(char (+ % (int \a))) (range 26))))

(defn encoding-pair
  "Takes a position (number 0-25) in the alphabet
      and returns the encoding pair ([a z], [b y], ...) for that position"
  [position]
  [(nth alphabet position) (nth alphabet (- (count alphabet) position 1))])

(def code-map (into {} (map encoding-pair (range 26))))

(defn map-transform
  "Takes a single input, returning the value if the input is a key in map_, otherwise returns the unchanged value"
  [input, map_]
  (if (contains? map_ input) (map_ input) input))

(defn encode [input-sequence]  (clojure.string/join (map #(map-transform % code-map), input-sequence)))
(def decode encode)

