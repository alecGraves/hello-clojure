(defproject hello-clojure "0.1.0-SNAPSHOT"
  :description "Me setting up a template clojure program"
  :url "http://gitlab.com/alecGraves/hello-clojure"
  :license {:name "MIT"
            :url "https://en.wikipedia.org/wiki/MIT_License"}
  :dependencies [[org.clojure/clojure "1.10.1"]]
  :repl-options {:init-ns hello-clojure.core}
  :main hello-clojure.core
  :aot [hello-clojure.core])
