# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## [Unreleased]
### Changed
- Add a new arity to `make-widget-async` to provide a different widget shape.

## [0.1.1] - 2020-10-26
### Changed
- Added unit test
- Added hello-clojure

## 0.1.0 - 2020-10-26
### Added
- Files from the new template.

[Unreleased]: https://github.com/your-name/hello-clojure/compare/0.1.1...HEAD
[0.1.1]: https://github.com/your-name/hello-clojure/compare/0.1.0...0.1.1
